%define debug_package %{nil}
%global openHarmony_source_release OpenHarmony-v3.2-Release
%global samgr_dir %{_builddir}/foundation/systemabilitymgr
%global build_opt /opt/distributed-middleware-build
%global samgr_path /openeuler/compiler_gn/foundation/systemabilitymgr/samgr

Name: systemabilitymgr_samgr
Version: 1.0.0
Release: 2
Summary: System ability manager
License: Apache License 2.0
Url: https://gitee.com/openharmony/systemabilitymgr_samgr
Source1: https://gitee.com/openharmony/systemabilitymgr_samgr/repository/archive/%{openHarmony_source_release}.tar.gz #/systemabilitymgr_samgr-%{openHarmony_source_release}.tar.gz
Source2: bundle.json
Source3: samgr_common.gn
Source4: samgr_proxy.gn

Patch1: 0001-adapt-compilation-for-samgr.patch

BuildRequires:  gcc, make, hilog, kernel-devel, libboundscheck
BuildRequires:  libatomic,libicu-devel, libxml2-devel, cjson-devel
BuildRequires:  distributed-build, distributed-beget
BuildRequires:  commonlibrary_c_utils
BuildRequires:  notification_eventhandler
BuildRequires:  communication_ipc

Requires: commonlibrary_c_utils
Requires:  distributed-beget
Requires:  notification_eventhandler
Requires: communication_ipc

%description
OpenEuler supports samgr for distributed softbus capability

%prep
rm -rf %{_builddir}/*
cp -rf %{build_opt} %{_builddir}/build
[ ! -L "%{_builddir}/build.sh" ] && ln -s %{_builddir}/build/build_scripts/build.sh %{_builddir}/build.sh
[ ! -L "%{_builddir}/.gn" ] && ln -s %{_builddir}/build/core/gn/dotfile.gn %{_builddir}/.gn
[ ! -L "%{_builddir}/build.py" ] && ln -s %{_builddir}/build/lite/build.py %{_builddir}/build.py
cp -rf %{_builddir}/build/openeuler/vendor %{_builddir}/
cp -rf %{_builddir}/build/openeuler/compiler_gn/* %{_builddir}

# 统一用setup，不用autosetup，distributed-build除外
mkdir -p %{samgr_dir}
%setup -q -D -T -a 1 -c -n %{samgr_dir}
mv systemabilitymgr_samgr-%{openHarmony_source_release}  %{samgr_dir}/samgr
# patch命令统一使用-P来指定第几个patch
%patch -P1 -p1 -d %{samgr_dir}/samgr

%build
rm -rf %{_builddir}/out

%ifarch x86_64
bash %{_builddir}/build.sh --product-name openeuler --target-cpu x86_64
%endif

%ifarch aarch64
bash %{_builddir}/build.sh --product-name openeuler --target-cpu arm64
%endif

%install
install -d -m 0755 %{buildroot}/%{_includedir}/samgr
install -d -m 0755 %{buildroot}/%{_libdir}
install -d -m 0755 %{buildroot}/usr/bin/
install -d -m 0755 %{buildroot}/system/bin/
install -d -m 0755 %{buildroot}/system/lib64/
install -d -m 0755 %{buildroot}/%{build_opt}%{samgr_path}/interfaces/innerkits/common/
install -d -m 0755 %{buildroot}/%{build_opt}%{samgr_path}/interfaces/innerkits/samgr_proxy/
install -d -m 0755 %{buildroot}/%{build_opt}%{samgr_path}/services/lsamgr/

%ifarch aarch64
%define module_out_path out/openeuler/linux_clang_arm64
%endif
%ifarch x86_64
%define module_out_path out/openeuler/linux_clang_x86_64
%endif

# copy executable file.
install -m 755 %{_builddir}/out/openeuler/packages/phone/system/bin/samgr %{buildroot}/system/bin/
install -m 755 %{_builddir}/out/openeuler/packages/phone/system/bin/samgr %{buildroot}/usr/bin/

# prepare so
install -m 0755 %{_builddir}/%{module_out_path}/systemabilitymgr/samgr/libsamgr*.so %{buildroot}/%{_libdir}
install -m 0755 %{_builddir}/%{module_out_path}/systemabilitymgr/samgr/libsamgr*.so %{buildroot}/system/lib64/

# prepare head files
# TODO: 头文件范围和bundle.json声明的不一致
install -m 554 %{samgr_dir}/samgr/services/lsamgr/include/*.h %{buildroot}/%{_includedir}/samgr
install -m 554 %{samgr_dir}/samgr/interfaces/innerkits/common/include/*.h %{buildroot}/%{_includedir}/samgr
install -m 554 %{samgr_dir}/samgr/interfaces/innerkits/samgr_proxy/include/*.h %{buildroot}/%{_includedir}/samgr

# install bundle.json and BUILD.gn which is used by other components.
install -m 554 %{SOURCE2} %{buildroot}/%{build_opt}%{samgr_path}/bundle.json
install -m 554 %{SOURCE3} %{buildroot}/%{build_opt}%{samgr_path}/interfaces/innerkits/common/BUILD.gn
install -m 554 %{SOURCE4} %{buildroot}/%{build_opt}%{samgr_path}/interfaces/innerkits/samgr_proxy/BUILD.gn

# gernerate soft link to samgr header path
ln -s /usr/include/samgr %{buildroot}/%{build_opt}%{samgr_path}/interfaces/innerkits/samgr_proxy/include
ln -s /usr/include/samgr %{buildroot}/%{build_opt}%{samgr_path}/interfaces/innerkits/common/include
ln -s /usr/include/samgr %{buildroot}/%{build_opt}%{samgr_path}/services/lsamgr/include

%files
%{_includedir}/samgr/*
%{_libdir}/*.so
/system/*
/usr/bin/*
%{build_opt}%{samgr_path}

%changelog
* Mon Apr 1 2024 s_c_c <shichuchao@huawei.com> - 1.0.0-2
- Fix samgr include_dirs for embedded

* Wed Jun 28 2023 tianhang <tian_hang@hoperun.com> - 1.0.0-1
- Init and adapt to openeuler

